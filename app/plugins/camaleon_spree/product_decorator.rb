class CamaleonCms::PostDecorator < CamaleonCms::ApplicationDecorator
  include CamaleonCms::CustomFieldsConcern
  delegate_all
end


Spree::User.class_eval do
  include CamaleonCms::UserMethods
end
